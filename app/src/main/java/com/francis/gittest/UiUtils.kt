package com.francis.gittest

import android.util.Log

object UiUtils {

    internal fun log(TAG: String?, message: String?) {
        Log.v("$TAG", "$message")
    }
}